package cn.jsj.gratuatepager;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 卢斌
 */
@SpringBootApplication
@MapperScan("cn.jsj.gratuatepager.dao")
public class GratuatepagerApplication {
    //可以联系本人Q：2695430372，我可以帮忙部署项目,本代码迭代在Gitee平台，如果你是在CSDN通过付费下载的本套代码明你被骗了。

    public static void main(String[] args) {
        SpringApplication.run(GratuatepagerApplication.class, args);
    }

}
